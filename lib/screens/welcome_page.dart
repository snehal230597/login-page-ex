import 'package:flutter/material.dart';

class WelcomePage extends StatefulWidget {
  String email, password;

  WelcomePage({required this.email, required this.password});

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  TextEditingController _email2 = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    _email2.text = widget.email;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 50),
            Text(
              'Email: ${widget.email}',
              style: TextStyle(fontSize: 18),
            ),
            Text(
              'Password: ${widget.password}',
              style: TextStyle(fontSize: 16, color: Colors.red),
            ),
            Padding(
              padding: const EdgeInsets.all(15),
              child: TextFormField(
                controller: _email2,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter email id';
                  }
                  if (value.contains('@')) {
                    return null;
                  } else {
                    return 'Not valid email';
                  }
                },
                textInputAction: TextInputAction.go,
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                obscureText: false,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: "Email",
                  labelStyle: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    fontFamily: "SegoeUI",
                    fontStyle: FontStyle.normal,
                  ),
                ),
              ),
            ),
            Center(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pop(
                    context,
                    _email2.text,
                  );
                },
                child: Text('Go Back'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
